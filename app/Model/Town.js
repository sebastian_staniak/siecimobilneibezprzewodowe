function Town (id, posX, posY) {
    this.id   = id;
    this.posX = posX;
    this.posY = posY;
    this.neighbours;

    this.setNeighbours = function (neighbours)
    {
        this.neighbours = neighbours;
    }

    this.getRandomNeighbour = function ()
    {
        var n = Math.floor((Math.random() * neighbours.length ));
        var neighbour = this.neighbours[n];

        return neighbour;
    }

    this.getBestNeighbour = function ()
    {
        // Set randomly value from 0.0 to 1.0
        var value;

        while(value = (Math.random() * (1.0 - 0.0) + 0.0)) {
            for (var i = 0; i < this.neighbours.length; i++) {
                if (this.neighbours[i][0] < value) {
                    // EXIT
                    return this.neighbours[i];
                }
            }
        }

        // ERROR NO NEIGHBOURS ?!
        return null;
    }
}