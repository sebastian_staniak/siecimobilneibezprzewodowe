function TownDrawerService (context) {
    this.context = context;

    /**
     *
     * @param town1
     *
     * Print town1
     */
    this.printTown = function(town1)
    {
		
		markLastTown(town1);
		markFirstTown(town1);
		
        var radius = 5;
        context.beginPath();
        context.arc(town1.posX, town1.posY, radius, 0, 2 * Math.PI, false);
        context.fillStyle = 'green';
        context.fill();
        context.lineWidth = 3;
        context.strokeStyle = '#003300';
        context.stroke();
    }
	markLastTown = function(town1){
		if(town1.posX == data[0][0].posX && town1.posY == data[0][0].posY){//town[0][0] coordinates
			context.font="15px Arial";
			context.fillStyle = 'red';
			context.fillText("FINISH HERE!",data[0][0].posX-50,data[0][0].posY-20);
			
		}
	}
	markFirstTown = function(town1){
		xVal = document.getElementById('startX').value;
		yVal = document.getElementById('startY').value;
		if(town1.posX == data[xVal][yVal].posX  && town1.posY == data[xVal][yVal].posY){//town[0][0] coordinates
			context.font="10px Arial";
			context.fillStyle = 'blue';
			context.fillText("START HERE!",data[xVal][yVal].posX-30,data[xVal][yVal].posY+20);			
		}
	}

    /**
     *
     * @param town1
     * @param town2
     *
     * Prints line between town1 and town2
     */
    this.printLineBetweenTowns = function (town1, town2, i)
    {
        context.beginPath();
        context.moveTo(town1[1].posX, town1[1].posY);
        context.lineTo(town2[1].posX, town2[1].posY);
        context.stroke();

        context.font="12px Arial";
        context.fillText(i.toString(), (town1[1].posX + town2[1].posX) / 2 , (town1[1].posY + town2[1].posY) / 2);
    }
}