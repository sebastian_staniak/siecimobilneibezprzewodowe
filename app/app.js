(function (document, points, TownDrawerService){
    function sleepFor( sleepDuration ){
        var now = new Date().getTime();
        while(new Date().getTime() < now + sleepDuration){ /* do nothing */ }
    }


    var canvas = document.getElementById('map');
    var button = document.getElementById('print');
    var context = canvas.getContext('2d');
    var drawer = new TownDrawerService(context);


    button.onclick = function () {
		if(!isValidData()){
			return null;
		}
        printLines();
    };

	function isValidData(){
		xLimit = data[0].length-1;//from list of towns
		yLimit = data.length-1;
		xVal = document.getElementById('startX').value;
		yVal = document.getElementById('startY').value;
		if(xVal==="" || yVal ===""){
			alert("Fields cannot be blank!")
			return false;
		}
		if(isNaN(xVal) || xVal < 0 || xVal > xLimit){
			alert("Check the x parameter!");
			return false;
		}
		if(isNaN(yVal) || yVal < 0 || yVal > yLimit){
			alert("Check the y parameter!");
			return false;
		}
		return true;
	};
    /**
     * draw 25 dots with stroke
     */

    function getRandoms()
    {
        var a = Math.floor((Math.random() * 5));
        var b = Math.floor((Math.random() * 5));

        return [a, b];
    }

    function printLines ()
    {
        context.clearRect(0, 0, canvas.width, canvas.height);

        for (var row in points) {
            for (var element in points[row]){
                var town = points[row][element];

                drawer.printTown(town);
            }
        }
        var i =0;
        do {
            var neighbour;
            var town2;
            var town1;


            if (town1 === undefined) {
                var x = document.getElementById('startX').value;
                var y = document.getElementById('startY').value;
                town1 = new Array();
                if (x === undefined || y === undefined) {
                    var r = getRandoms();
                } else {
                    r = [x, y];
                }
                    town1[1] = points[r[0]][r[1]];
                    //town2 = town1[1].getRandomNeighbour();
                    town2 = town1[1].getBestNeighbour();
            } else {
                town1[1] = town2[1];
                //town2 = town1.getRandomNeighbour();
                town2 = town1[1].getBestNeighbour();
            }
            i++;

            drawer.printLineBetweenTowns(town1, town2, i);
        } while (town2[1].id !== 0);
    }

})(document, data, TownDrawerService);