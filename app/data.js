var data = [
    [
        new Town(0, 50,50),
        new Town(1, 50,150),
        new Town(2, 50,250),
        new Town(3, 50,350),
        new Town(4, 50,450),
    ],
    [
        new Town(5, 150,50),
        new Town(6, 150,150),
        new Town(7, 150,250),
        new Town(8, 150,350),
        new Town(9, 150,450),
    ],
    [
        new Town(10, 250,50),
        new Town(11, 250,150),
        new Town(12 ,250,250),
        new Town(13, 250,350),
        new Town(14 ,250,450),
    ],
    [
        new Town(15, 350,50),
        new Town(16, 350,150),
        new Town(17, 350,250),
        new Town(18, 350,350),
        new Town(19, 350,450),
    ],
    [
        new Town(20, 450,50),
        new Town(21, 450,150),
        new Town(22, 450,250),
        new Town(23, 450,350),
        new Town(24, 450,450),
    ],
];

for (var row = 0; row < data.length; row++) {
    for (var col = 0; col < data.length; col++) {
        var town = data[row][col];
        var neighbours = [];

        if (col > 0) {
            // Set probability for exits
            var probability = (Math.random() * (1.0 - 0.0) + 0.0);
            neighbours.push([probability, data[row][col - 1]]);
        }

        if (row > 0) {
            // Set probability for exits
            var probability = (Math.random() * (1.0 - 0.0) + 0.0);
            neighbours.push([probability, data[row - 1][col]]);
        }

        if (col < 4) {
            // Set probability for exits
            var probability = (Math.random() * (1.0 - 0.0) + 0.0);
            neighbours.push([probability, data[row][col + 1]]);
        }

        if (row < 4) {
            // Set probability for exits
            var probability = (Math.random() * (1.0 - 0.0) + 0.0);
            neighbours.push([probability, data[row + 1][col]]);
        }

        town.setNeighbours(neighbours);
    }
}
